Hey team I wanted to quickly share something about my working hours:

* I don’t keep “standard office hours”
* I mostly work unpredictable and non-linear workdays
* Sometimes I start early, sometimes late
* Sometimes I work in 3 shifts
* I do this because it works for me and my personal situation
* I certainly don’t do this because I feel I have to, I do it because I want to
* I sometimes work 4 long days and take it easy on the fifth.
* I like to take afternoon naps at times.

So why share this?

1. I don’t want you to think because I work odd hours and you might see me up late that there is any need for you to do so.
1. I certainly don’t expect you to respond to me outside your work hours just cause I might send you a message when I am working - I do it because I might forget - and will start using Slack’s send later feature once it lands.
1. But most of all I want you to know that it is also OK for you to work in the way that works for you and your team. We don’t measure your inputs and don’t care how long or when you are online on a particular day. Just make sure you work sustainably based on what works for you
